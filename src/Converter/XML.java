package Converter;

import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;

public class XML implements Extensions {

	// ==========================================================
	//
	// XML
	//
	// ==========================================================

	public void importFile(File f) {
		if (!f.exists()) {
			System.err.println("File not Found!");
			return;
		}
		ArrayList<String> ticketParams = new ArrayList<String>();
		ArrayList<Ticket> importedTickets = new ArrayList<Ticket>();
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			// read until first ticket is found
			while ((line = br.readLine()) != null) {
				if (line.contains("<ticket>")) {
					break;
				}
			}
			// import all other tickets tickets
			outerloop: while ((line = br.readLine()) != null) {
				// in this loop, we're inside the ticket element

				// if the end of the ticket is found, create a ticket using the collected parameters in the list
				// and add it to the list
				if (line.contains("</ticket>")) {
					// create the ticket from params in external function
					importedTickets.add(createTicket(ticketParams));
					// try to find the next ticket element
					while ((line = br.readLine()) != null) {
						if (line.contains("<ticket>")) {
							continue outerloop;
						}
					}
					// if no new ticket element is found, the loop will read until the end of the file
					// at end of file is no more line so we break the main outer loop here
					if (line == null) {
						break outerloop;
					}

				}

				// if all the above conditions do not apply, the read line is an attribute of the ticket
				// if that is the case, jump directly to the elements beginning by searching for the <
				if (line.contains("<")) {
					// if found, ignore everything that comes before it
					line = line.substring(line.indexOf("<"));
				}
				// add the collected, santized ticket parameter to the params list

				ticketParams.add(line);
			}

			// loop over the generated tickets and add them to the internal storage
			for (int i = 0; i < importedTickets.size(); i++) {
				Storage.addTicket(importedTickets.get(i));
			}
			System.out.println(importedTickets.size() + " Tickets imported!");

		} catch (Exception e) {
			System.err.println("Import Error!");
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	// Parse to Ticket
	private Ticket createTicket(ArrayList<String> al) {

		// initialize neede for Ticket creation
		String title = "";
		String description = "";
		String customer = "";
		int state = 1;
		long dateIn = 0;
		Timestamp datetime = null;
		// loop over every parameter in the provided list and determine, which attribute it is
		for (int i = 0; i < al.size(); i++) {
			String s = al.get(i);
			//if the provided parameter matches one of the ticket parameters,
			//remove the xml element tags and store the value
			if (s.contains("<title>") && s.contains("</title>")) {
				title = s.replace("<title>", "").replace("</title>", "");

			} else if (s.contains("<description>") && s.contains("</description>")) {
				description = s.replace("<description>", "").replace("</description>", "");
				
			} else if (s.contains("<customer>") && s.contains("</customer>")) {
				customer = s.replace("<customer>", "").replace("</customer>", "");

			} else if (s.contains("<state>") && s.contains("</state>")) {
				state = Integer.parseInt(s.replace("<state>", "").replace("</state>", ""));

			} else if (s.contains("<datetime>") && s.contains("</datetime>")) {
				dateIn = Long.parseLong(s.replace("<datetime>", "").replace("</datetime>", ""));
				datetime = new Timestamp(dateIn);
			}
		}

		// create from the attributes the ticket and store them ito the storage.
		return new Ticket(title, description, customer, state, datetime);
	}

	public void exportFile(File f) {
		try {
			FileWriter fw = new FileWriter(f);
			//write the xml descriptor
			fw.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
			//make a new line
			fw.write(System.lineSeparator());
			//signal beginning of root element
			fw.write("<ticketList>");
			fw.write(System.lineSeparator());
			//iterate over tickets
			for (Ticket t : Storage.getTickets()) {
				//get the Timestamp for the Ticket beforehand
				String time = ""+t.getDatetime().getTime();
				//start ticket object element
				fw.write("<ticket>");
				//write all attributes into the object element
				fw.write(System.lineSeparator());
				fw.write("<title>"+t.getTitle()+"</title>");
				fw.write(System.lineSeparator());
				fw.write("<description>"+t.getDescription()+"</description>");
				fw.write(System.lineSeparator());
				fw.write("<customer>"+t.getCustomer()+"</customer>");
				fw.write(System.lineSeparator());
				fw.write("<state>"+t.getState()+"</state>");
				fw.write(System.lineSeparator());
				fw.write("<datetime>"+time+"</datetime>");
				fw.write(System.lineSeparator());
				
				//end ticket object element
				fw.write("</ticket>");
				fw.write(System.lineSeparator());
			}
			//write end of root element
			fw.write("</ticketList>");
			//close the file writer
			fw.close();
		} 
		catch (Exception e) {
			System.err.println("Export Error!");
			System.err.println(e.getLocalizedMessage());
		}
	}

}
