package Converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Json implements Extensions{


	// ==========================================================
	//
	// JSON
	//
	// ==========================================================
	// the following code is responsible for importing and exporting the given files to the json format
	
	// method to export to valid JSON
	public void exportFile(File f) {
		//start the String that needs to be exported
		String contents = "";
		//open the array, our data structure starts with
		contents += "[";
		//iterate over the entire Ticket storage. For each Ticket:
		for(Ticket ticket : Storage.getTickets()) {
			//open the object
			contents+="{";
			//add each key value pair to object (" needs to be escaped into \" as to not confuse the compiler)
			contents += "\"title\":\"";
			contents += ticket.getTitle();
			contents += "\",";
			
			contents += "\"description\":\"";
			contents += ticket.getDescription();
			contents += "\",";
			
			contents += "\"customer\":\"";
			contents += ticket.getCustomer();
			contents += "\",";
			
			contents+="\"state\":"+ticket.getState()+",";
			contents+="\"datetime\":"+ticket.getDatetime().getTime();
			//close the object and add , to signal next object
			contents+="},";
		}
		//delete the last "," to end the listing
		contents=contents.substring(0,contents.length()-1);
		//close the array
		contents += "]";
		//try to export the file
		try {
			//open a File Writer
			FileWriter fw = new FileWriter(f);
			//write the contents into the file
			fw.write(contents);
			//close the File Writer
			fw.close();
		} catch (Exception e) {
			//exception handling borrowed from CSV.Java
			System.err.println("Export Error!");
			System.err.println(e.getLocalizedMessage());
		}
		
	}
	
	
	// method for importing Tickets from a file
	public void importFile(File f) {
		//extract the contents of the File into a single string
		String file_content = readFile(f);
		//create a List for the JSON objects
		ArrayList<String> json_objects = extractObjects(file_content);
		System.out.println("Found "+json_objects.size() + " objects");
		//iterate over the Object list. For each object:
		for(String json_object : json_objects) {
			//remove all the whitespace from the object
			//turns out this also removes spaces from the ticket contents so it'll stay out for now
			//json_object = json_object.replaceAll("\\s", "");
			
			//prepare all attributes for Ticket creation			
			String title = extract_string_value(extract_nv_pair(json_object, "title"));
			
			String description = extract_string_value(extract_nv_pair(json_object, "description"));
			
			String customer = extract_string_value(extract_nv_pair(json_object, "customer"));
			
			int state = extract_int_value(extract_nv_pair(json_object, "state"));
			
			long datetime = extract_long_value(extract_nv_pair(json_object, "datetime"));
			//needs to be converted to a Timestamp Object first
			Timestamp ticket_timestamp = new Timestamp(datetime);
			
			//create the Ticket using the previously extracted attributes
			Ticket t = new Ticket(title, description, customer, state, ticket_timestamp);
			//add the Ticket to the Storage
			Storage.addTicket(t);
		}
	}
	
	//------------------------------------------------
	// Helper Methods for importFile
	//------------------------------------------------
	//these methods contain the functions the importFile-Method calls
	
	//Method for reading the contents of the file into a single String
	private String readFile(File f){
		//initialize the String that will hold all the contents of the file
		String contents = ""; 
		//try to read the file
		try {
			//initialize String for contents in current line
			String current_line = "";
			//initialize FileReader for interaction with the File
			FileReader fr = new FileReader(f);
			//initialize a Buffered reader for the actual reading
			BufferedReader content_reader = new BufferedReader(fr);
			//read first line
			current_line = content_reader.readLine();
			//read all the lines until no new lines are available
			//at which point the value is NULL
			while(current_line != null) {
				//add the current line to the file content
				contents += current_line;
				//read the next line
				current_line = content_reader.readLine();
			}
			//close both readers
			content_reader.close();
			fr.close();
		} catch (IOException e) {
			//print error Message if reading fails and print stacktrace
			System.out.println("Error importing File");
			e.printStackTrace();
		}
		return contents;
	}
	
	// Method for extracting JSON objects out of the main array
	// It returns them as a List
	private ArrayList<String> extractObjects(String json_content){
		//Create a list for the JSON-Objects
		ArrayList<String> json_objects = new ArrayList<String>();
		//Compile the regex pattern for matching the objects
		//For regex explanation see doc/json/JSON_Struktur_und_regex.md 
		//Extra escape chars are needed because of Java's Strings. Our Java IDE (Eclipse) automatically inserts them when pasting
		Pattern pattern = Pattern.compile("\\{[^\\}]*\\}");
		//create the matcher for matching objects in the json_content string using the pattern
		Matcher matcher = pattern.matcher(json_content);
		//find all the matches throughout the string
		//if there is no match, return an empty string
		while(matcher.find()) {
			//add the contents of the found match to the list
			json_objects.add(matcher.group());
		}
		//return the filled list
		return json_objects;
	}
	
	//extract a specific name value pair from a given JSON object
	private String extract_nv_pair(String json_object,String name) {
		//initialize the return value so it does not break if nothing is found
		String value = "";
		//add the name for the pair to the pattern
		String keyValuePattern = "\""+name+"\":[^\\}\\],]*[\\}\\],]";
		//compile the pattern for extracting the specified key-value pair and add the matcher
		Pattern pattern = Pattern.compile(keyValuePattern);
		Matcher matcher = pattern.matcher(json_object);
		//find the key-value pair
		if(matcher.find()) {
			value = matcher.group();
		}
		else {
			//notify if it couldn't be found
			System.err.println("NV-Pair for "+ name + "not found");
		}
		return value;
	}
	
	//get a string value from a key value pair
	private String extract_string_value(String keyValuePair) {
		String value = "";
		//compile pattern for matching a string
		Pattern pattern = Pattern.compile("(?<=:\").*(?=\"[,}\\]])");
		Matcher matcher = pattern.matcher(keyValuePair);
		//try to find the string
		if(matcher.find()) {
			value = matcher.group();
		}
		else {
			//notify if it couldn't be found
			System.err.println("could not extract valid String");
		}
		return value;
	}
	
	private long extract_long_value(String keyValuePair) {
		long value = 0;
		//compile pattern for matching a number
		Pattern pattern = Pattern.compile("(?<=:)\\d*(?=[,}\\]])");
		Matcher matcher = pattern.matcher(keyValuePair);
		//try to find the string
		if(matcher.find()) {
			//convert the read number to a long
			value = Long.parseLong(matcher.group());
		}
		else {
			//notify if it couldn't be found
			System.err.println("could not extract valid number");
		}
		return value;
	}
	private int extract_int_value(String keyValuePair) {
		int value = 0;
		//compile pattern for matching a number
		Pattern pattern = Pattern.compile("(?<=:)\\d*(?=[,}\\]])");
		Matcher matcher = pattern.matcher(keyValuePair);
		//try to find the string
		if(matcher.find()) {
			//convert the found number into an int
			value = Integer.parseInt(matcher.group());
		}
		else {
			//notify if it couldn't be found
			System.err.println("could not extract valid number");
		}
		return value;
	}

}