package Converter;

import java.io.File;

public interface Extensions {

	// ==========================================================
	//
	// Interface Class
	//
	// ==========================================================
	
	public void importFile(File f);
	
	
	public void exportFile(File f);
	
}
