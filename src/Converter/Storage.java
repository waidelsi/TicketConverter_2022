package Converter;

import java.util.ArrayList;

public class Storage {

	// ==========================================================
	//
	// Storage Class to Store the Tickets
	//
	// ==========================================================

	// Arraylist to store the tickets
	private static ArrayList<Ticket> ticketlist = new ArrayList<Ticket>();

	// Add a Ticket
	public static void addTicket(Ticket ticket) {
		if (!ticketlist.contains(ticket)) {
			ticketlist.add(ticket);
		}
	}

	// get a specific Ticket
	public static Ticket getTicket(int ticketID) {
		return ticketlist.get(ticketID);
	}

	// get a Number how many Tickets
	public static int getAmountOfTicket() {
		return ticketlist.size();
	}

	// clear / delete al Tickets
	public static void clearTickets() {
		ticketlist.clear();
	}

	// Get Ticketlist
	public static Ticket[] getTickets() {
		Ticket t[] = new Ticket[ticketlist.size()];
		for (int i = 0; i < t.length; i++) {
			t[i] = ticketlist.get(i);
		}

		return t;

	}

}
