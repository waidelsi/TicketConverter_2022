package Converter;

import java.io.File;
import java.util.Scanner;

public class Menu {

	private static Scanner sc = new Scanner(System.in);

	public static void main() {

		int choose = 0;

		do {

			// Print the Main Menu
			System.out.println("==========================================");
			System.out.println("Chose your Option:");
			System.out.println("1 - Import File");
			System.out.println("2 - Export");
			System.out.println("3 - List Tickets");
			System.out.println("4 - Clear Tickets");
			System.out.println("5 - Generate Test Tickets");
			System.out.println("6 - Create a Ticket");
			System.out.println();
			System.out.println("0 - Exit");
			System.out.println("==========================================");

			// Read users input
			choose = sc.nextInt();

			// evaluate the users input and execute corresponding methods
			switch (choose) {
			case 1:
				importFile();
				break;
			case 2:
				exportFile();
				break;
			case 3:
				listTickets();
				break;
			case 4:
				clearTickets();
				break;
			case 5:
				// Generate Random / Test Tickets
				for (int i = 0; i < 25; i++) {
					Ticket tmp = new Ticket("title" + i, "description" + i, "customer" + i);
					Storage.addTicket(tmp);
				}
				System.out.println("Tickets generated!");
				break;
			case 6:
				createTicket();
				break;
			case 0:
				// Exit program
				System.out.println("Bye!");
				return;

			default:
				System.out.println("Unknown option.");
				break;
			}
			// Press Enter
			pressAnyKey();

		} while (choose != 0);
		System.out.println("Bye!");
		sc.close();
	}

	// =======================================================================
	//
	// --------------------------- [ List Ticket ] ---------------------------
	//
	// =======================================================================

	private static void listTickets() {

		// is no tickets avalible you return here.
		if (Storage.getAmountOfTicket() == 0) {
			System.out.println("There are no Tickets");
			return;
		}

		int choice = 0;
		do {
			// List 10 Tickets per Side

			// Calc Pages
			int maxPages = (int) Math.round((double) Storage.getAmountOfTicket() / 10 + 0.4);

			System.out.println("Max Pages: " + maxPages);
			System.out.println("Type 0 to go back to Menu.");
			// read page that the user will see.
			System.out.println("Page: ");
			choice = sc.nextInt();

			// 0 -> exit to main menu
			if (choice == 0)
				return;

			// Only Pages Between first and last Page
			if (choice < 1)
				choice = 1;

			if (choice > maxPages)
				choice = maxPages;

			// (choose - 1) * 10

			// check if the page is valid
			int startIndex = (choice - 1) * 10;
			System.out.println("=========== [ " + choice + " / " + maxPages + " ] ===========");
			// Print ticket data for 10 tickets per page
			for (int i = startIndex; i < Storage.getAmountOfTicket() && i < startIndex + 10; i++) {
				Ticket t = Storage.getTicket(i);
				System.out.println(" Name: " + t.getTitle());
				System.out.println("Custo: " + t.getCustomer());
				System.out.println(" Desc: " + t.getDescription());
				System.out.println(" Time: " + t.getDatetime().toString().split("\\.")[0]);
				System.out.println("State: " + (t.getState() + "").replace("1", "Open").replace("2", "In Progress").replace("3", "Closed"));
				System.out.println("------------------------------");
			}
			System.out.println("=========== [ " + choice + " / " + maxPages + " ] ===========");
		} while (choice != 0);

	}

	// =======================================================================
	//
	// -------------------------- [ Clear Tickets ] --------------------------
	//
	// =======================================================================

	private static void clearTickets() {
		System.out.println("To clear type uppercase 'yes'.");
		String override = "";

		while ((override = sc.nextLine()).length() < 1)
			;

		// close the scanner

		override = override.replace(" ", "");

		if (!override.equals("YES")) {
			System.out.println("Break!");
			return;
		}
		Storage.clearTickets();
		System.out.println("Tickets deleted!");

	}

	// =======================================================================
	//
	// --------------------------- [ Import File ] ---------------------------
	//
	// =======================================================================
	private static void importFile() {

		// prompt user for path to file
		System.out.println("Enter the path of your file:");
		String path = "";

		// read path from console - no empty path
		while ((path = sc.nextLine()).length() <= 1)
			;

		try {
			// create the file
			File f = new File(path);

			// check if file exists
			if (!f.exists()) {
				System.out.println("File does not exist.");

				return;
			}

			// extract file extension
			String fileparams[] = f.getName().split("\\.");
			String fileending = fileparams[fileparams.length - 1];

			Extensions ex = null;

			// determine the correct File Handler
			if (fileending.equalsIgnoreCase("csv")) {
				ex = new CSV();

			} else if (fileending.equalsIgnoreCase("json")) {
				ex = new Json();
			} else if (fileending.equalsIgnoreCase("xml")) {
				ex = new XML();
			} else {
				System.out.println("Not supported file ending.");
				// Press any key
				return;
			}
			// Import the file by calling the import method
			ex.importFile(f);

		} catch (Exception e) {
			System.out.println("File not found.");
		}

	}

	// =======================================================================
	//
	// --------------------------- [ Export File ] ---------------------------
	//
	// =======================================================================

	private static void exportFile() {
		System.out.println("Enter the path of your file:");
		String path = "";
		// prompt user for file path
		while ((path = sc.nextLine()).length() <= 1)
			;

		try {
			// create the file
			File f = new File(path);

			// when the file exists, ask the user if he really wants to override the file
			if (f.exists()) {
				System.out.println("File does exist. To override the file type uppercase 'yes'.");
				String override = "";
				while ((override = sc.nextLine()).length() <= 1)
					override = override.replace(" ", "");
				if (!override.equals("YES")) {
					System.out.println("Aborded.");
					return;
				}
				//if override was confirmed, delete the file
				f.delete();
			}

			// create file
			f.createNewFile();

			// extract file extension
			String fileparams[] = f.getName().split("\\.");
			String fileending = fileparams[fileparams.length - 1];

			Extensions ex = null;

			//determine the correct handler for the file extension
			if (fileending.equalsIgnoreCase("csv")) {
				ex = new CSV();

			} else if (fileending.equalsIgnoreCase("json")) {
				ex = new Json();
			} else if (fileending.equalsIgnoreCase("xml")) {
				ex = new XML();
			} else {
				System.out.println("Not supported file ending.");
				return;
			}
			// Export the file
			ex.exportFile(f);

			System.out.println("File exported!");

		} catch (Exception e) {
			System.err.println("File not found.");
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
		}

	}

	// =======================================================================
	//
	// -------------------------- [ Create Ticket ] --------------------------
	//
	// =======================================================================

	private static void createTicket() {

		// Ticket attributes
		String title = "";
		String customer = "";
		String description = "";

		System.out.println("Create a Ticket:");
		System.out.println();
		// Import the ticket contents from user input
		System.out.println("Title:");
		while ((title = sc.nextLine()).length() < 1)
			;
		System.out.println("Customer: ");
		while ((customer = sc.nextLine()).length() < 1)
			;
		System.out.println("Description:");
		while ((description = sc.nextLine()).length() < 1)
			;

		// create and store the ticket
		Ticket tmp = new Ticket(title, description, customer);
		Storage.addTicket(tmp);

		System.out.println("Ticket created!");

	}

	// method for breaking and letting the user continue by pressing enter
	private static void pressAnyKey() {

		System.out.println("Press Enter to continue...");

		try {
			System.in.read();
		} catch (Exception e) {
		}

	}

}
