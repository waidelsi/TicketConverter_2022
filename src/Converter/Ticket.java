package Converter;

//We are using SQL-Timestamps for an accurate and TimeZone independent Time and Date
import java.sql.Timestamp;

public class Ticket {

	// ==========================================================
	// 
	// 			Ticket Class to create Tickets
	//
	// ==========================================================
	
	private String title; //Ticket Title is Text -> String
	private String description; //Ticket Description -> Text -> String
	private String customer; //Customer name as String
	/*
	 * Ticket states are predefined to avoid collisions:
	 * 1: Open
	 * 2: In Process
	 * 3: Closed 
	 */
	private int state; //only a few states -> int
	/**
	 * 
	 */
	private Timestamp datetime; //Time and Date saved as TimeStamp
	
	public Ticket(String title, String description, String customer) {
		//title, desc. and customer set by arguments
		this.title = title;
		this.description = description;
		this.customer = customer;
		this.state = 1; //Default State is "Open"
		this.datetime = new Timestamp(System.currentTimeMillis()); //Timestamp of the exact time and date in unix-millis when the Ticket is created
	}
	
	public Ticket(String title, String description, String customer, int state, Timestamp datetime) {
		//title, desc. and customer set by arguments
		this.title = title;
		this.description = description;
		this.customer = customer;
		this.state = 1;
		this.datetime = datetime;
	}
	
	//from here just generic getters and setters for Title, Description, Customer and State

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Timestamp getDatetime() {
		return datetime;
	}

	public void setDatetime(Timestamp datetime) {
		this.datetime = datetime;
	}
	
	
	
	
	
}
