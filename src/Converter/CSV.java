package Converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;

public class CSV implements Extensions {
// Doh
// Slant

//          CCCCCCCCCCCCC     SSSSSSSSSSSSSSS   VVVVVVVV           VVVVVVVV
//       CCC::::::::::::C   SS:::::::::::::::S  V::::::V           V::::::V
//     CC:::::::::::::::C  S:::::SSSSSS::::::S  V::::::V           V::::::V
//    C:::::CCCCCCCC::::C  S:::::S     SSSSSSS  V::::::V           V::::::V
//   C:::::C       CCCCCC  S:::::S               V:::::V           V:::::V 
//  C:::::C                S:::::S                V:::::V         V:::::V  
//  C:::::C                 S::::SSSS              V:::::V       V:::::V   
//  C:::::C                  SS::::::SSSSS          V:::::V     V:::::V    
//  C:::::C                    SSS::::::::SS         V:::::V   V:::::V     
//  C:::::C                       SSSSSS::::S         V:::::V V:::::V      
//  C:::::C                            S:::::S         V:::::V:::::V       
//   C:::::C       CCCCCC              S:::::S          V:::::::::V        
//    C:::::CCCCCCCC::::C  SSSSSSS     S:::::S           V:::::::V         
//     CC:::::::::::::::C  S::::::SSSSSS:::::S            V:::::V          
//       CCC::::::::::::C  S:::::::::::::::SS              V:::V           
//          CCCCCCCCCCCCC   SSSSSSSSSSSSSSS                 VVV  

//      ______  _______  ____  ____  ______
//     /  _/  |/  / __ \/ __ \/ __ \/_  __/
//     / // /|_/ / /_/ / / / / /_/ / / /   
//   _/ // /  / / ____/ /_/ / _, _/ / /    
//  /___/_/  /_/_/    \____/_/ |_| /_/     
//	

	public void importFile(File f) {

		if (!f.exists()) {
			System.err.println("File not Found!");
			return;
		}

		int ticketCounter = 0;
		try {
			// create readers to read the file lines.
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			// titles -> topics
			// read the first line to get the Topic items
			String topicLine = br.readLine();

			// Convert topicLine to Arraylist
			ArrayList<String> topics = stringArrtoAL(topicLine.split(";"));

			// customer;title;description;state;date

			// read lines from file
			String line = "";
			while ((line = br.readLine()) != null) {

				// customer;title;description;state;date
				// seperate / parse ticket items to single string objects by ;
				String params[] = line.split(";");

				// error prevention
				// we need almost 5 items in the list
				if (params.length < 5)
					continue;
				// String title, String description, String customer, int state, Timestamp
				// datetime

				// extract the ticket items from params to single attributes
				String title = params[topics.indexOf("title")];
				String description = params[topics.indexOf("description")];
				String customer = params[topics.indexOf("customer")];
				int state = Integer.parseInt(params[topics.indexOf("state")]);
				long dateIn = Long.parseLong(params[topics.indexOf("date")]);
				Timestamp datetime = new Timestamp(dateIn);

				// String title, String description, String customer, int state, Timestamp
				// datetime
				// create from the attributes the ticket and store them ito the storage.
				Ticket t = new Ticket(title, description, customer, state, datetime);
				Storage.addTicket(t);

				// count tickets
				ticketCounter++;
			}
			System.out.println(ticketCounter + " Tickets imported!");
			br.close();
		} catch (Exception e) {
			// print errors
			// that the user knows whats happend
			System.err.println("Import Error!");
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
		}

	}

//      _______  __ ____  ____  ____  ______
//     / ____/ |/ // __ \/ __ \/ __ \/_  __/
//    / __/  |   // /_/ / / / / /_/ / / /   
//   / /___ /   |/ ____/ /_/ / _, _/ / /    
//  /_____//_/|_/_/    \____/_/ |_| /_/     
//                                        

	public void exportFile(File f) {
		try {
			// create filewriter to write into the file
			FileWriter fw = new FileWriter(f);

			// Title
			// write the title
			fw.write("customer;title;description;state;date");
			// Write new Line
			fw.write(System.lineSeparator());

			// write the tickets into seperated lines
			for (Ticket t : Storage.getTickets()) {
				// extract the datetime to string
				String time = "" + t.getDatetime().getTime();
				// write the ticket into the current line
				fw.write(t.getCustomer() + ";" + t.getTitle() + ";" + t.getDescription() + ";" + t.getState() + ";" + time);
				// new Line
				fw.write(System.lineSeparator());

			}

			// close the File Writer
			fw.close();

		} catch (Exception e) {
			System.err.println("Export Error!");
			System.err.println(e.getLocalizedMessage());
		}

	}

//	   ____  ________  ____________ 
//	   / __ \/_  __/ / / / ____/ __ \
//	  / / / / / / / /_/ / __/ / /_/ /
//	 / /_/ / / / / __  / /___/ _, _/ 
//	 \____/ /_/ /_/ /_/_____/_/ |_|  
//	                                 	

	// Convert String Array to Arraylist
	private static ArrayList<String> stringArrtoAL(String in[]) {
		ArrayList<String> al = new ArrayList<String>();
		for (int i = 0; i < in.length; i++) {
			al.add(in[i]);
		}

		return al;
	}
}
