# Ticket Converter 2022

Gruppenarbeit SAE: Ticketsystem mit Export und Importfunktionen für CSV,XML und JSON

## Ziel

Entwicklung eines konsolenbasierten Ticketsystem. Es soll zwei Anforderungen erfüllen

1. Erstellen und Auslesen von Tickets

2. Import und Export von Tickets in folgende Formate
   
   1. CSV
   
   2. XML
   
   3. JSON

## festegelegte Konventionen

### Programmiersprache: Java

Wir haben uns für die Programmiersprache Java entschieden, weil alle Gruppenmitglieder bereits mit der Sprache vertraut waren und sie für die Zusammenarbeit in einer gemeinsamen Codebase gut geeignet ist.

Der Code ist in englisch dokumentiert.

### Source Control: Git

Um möglichst reibungsloses Zusammenarbeiten zu ermöglichen haben wir zur Verwaltung von Änderungen und Zusammenfürhung von den jeweiligen Teilaufgaben git verwendet.

Zum start des Projektes haben wir uns gemeinsam ein Konzept überlegt und ein Skelettprogramm implementiert. Von dort wurde das Projekt in drei branches aufgeteilt: master, xml und json. Am Ende wurden diese wieder zu einem Projekt zusammengeführt.

Gehostet wurde das Projekt unter https://codeberg.org/waidelsi/TicketConverter_2022

### IDE: Eclipse

Als IDE wurde [Eclipse IDE](https://www.eclipse.org/) verwendet. Das Programm wurde hier als normales Java Projekt angelegt. Es wurde Java 17 verwendet.

## Dokumentation

Die Dokumentation des Projektes liegt im Projektordner unter `/doc`

Hier sind direkte Links zu den jew. Teilen:

[Projektstruktur](doc/projektstruktur/Projektstrukur_TicketConverter_2022.md)

[Menu](doc/menu/menu.md)

[CSV](doc/csv/CSV_doku.md)

[JSON-Code](doc/json/Programmablauf.md)

[JSON-Struktur](doc/json/JSON_Struktur_und_Regex.md)

[XML-Code](doc/xml/Programmablauf.md)

[XML-Struktur](doc/xml/Aufbau_Ticket_XML.md)

## Importieren des Projektes

Zum importieren des Projektes gibt es zwei Möglichkeiten

1. Importieren direkt von Codeberg
   
   1. URL kopieren: `https://codeberg.org/waidelsi/TicketConverter_2022.git`
   
   2. In Eclipse Datei - Importieren anwählen
   
   3. Unter git - Projekte von Git anwählen und weiter
   
   4. Clone URL und weiter
   
   5. URL einfühgen und zwei mal weiter
   
   6. Inital Branch auf die jew. Wunsch-Branch (master) legen und weiter
   
   7. Auf fertigstellen drücken

2. Importieren via Zip
   
   1. In Eclipse Datei - Importieren anwählen
   
   2. General - Projekte aus Ordner/Archiv
   
   3. Archiv... drücken und auswählen
   
   4. Haken beim kürzeren Namen entfernen und auf Fertigstellen drücken

## Ausführen des Projektes

Zum ausführen des Projektes gibt es zwei Möglichkeiten:

1. Über die IDE
   
   1. Projekt öffnen und unter `src/Converter/Programmstart.java` öffnen
   
   2. auf "Run" (grüner Play Knopf) drücken

2. Öffnen der kompilierten jar-Datei
   
   1. Kommandozeile öffnen
   
   2. `/pfad/zu/java17 --jar TicketConverter_2022.jar ausführen`

## Aufgabenzuteilung

Die Aufgen wurden wiefolgt zugeteilt.

- Aron
  
  - Entwickeln des Hauptprogramms
  
  - Erstellen des CSV-Handlers

- Lukas
  
  - Erstellen des XML-Handlers
  
  - Dokumentation

- Simon
  
  - Erstellen des JSON-Handlers
  
  - Source Control
  
  - Dokumentation

## Aufgabe

Aufgabenstellung aus: Moodle Kurs, E2FS2T_SCA_SAE_22/23, Projekt Ticketsystem

### Ticketsystem

Ihr entwickelt in Gruppe von zwei bis drei Teilnehmern ein kleines 
konsolenbasiertes Ticketsystem. Hauptaugenmerk ist die sog. 
Interoperabilität. Das bedeutet, dass System soll mit bis zu drei 
unterschiedlichen Dateiformaten zur Speicherung und Wiederherstellung 
der Tickets arbeiten können.

🤔 Sinn und Zweck dieses Projektes ist nicht nur die PK-Notenfindung,
 sondern dass Ihr euch mit den Datenformaten CSV, JSON und XML 
auseinandersetzt.  

Da alle drei Formate für einen zu viel sind, ist die Idee, dass jeder im Team sich mit genau einem Format befasst. <u>Solltet Ihr nur zu zweit sein, lasst das Format CSV weg.</u>

Die Anwendung soll folgendermaßen Funktionieren:

- Nach dem Starten der Anwendung kann der Benutzer eine Datei 
  angeben, die geladen werden soll. Je nach Dateiendung wird ein 
  entsprechender Code aufgerufen, um die drei unterschiedlichen 
  Dateiformate zu lesen (.txt, .json, .xml). Wir keine Datei angegeben, 
  wird keine Datei geladen. In den Dateien stehen eventuell vorher 
  gespeicherte Ticketinformationen.
- Der Anwender hat nach dem laden die Möglichkeit, alle Tickets anzuzeigen oder ein neues Ticket zu erstellen.
- Wir ein neues Ticket erstellt, sollen nacheinander folgende 
  Informationen eingegeben werden können: Titel, Autor des Tickets und 
  Beschreibung. Zusätzlich werden zwei weitere Informationen automatisch 
  generiert: Erstellungsdatum und Status (offen). Ich weis, den Status 
  usw. kann man so erst einmal nicht ändern, das müssen wir so 
  akzeptieren.
- Der Anwender kann auswählen, alle Tickets in eine Datei zu 
  speichern. Selbes hier, je nach Dateiendung wird das entsprechende 
  Format ausgewählt.

#### Folgende Punkte sind zu bearbeiten:

1. Ihr entwerft gemeinsam ein Konzept, wie die Anwendung 
   grundsätzlich aussehen soll und welche Programmiersprache zum Einsatz 
   kommt. Überlegt euch ein modulares Konzept, so dass der Export 
   unabhängig von der Gemeinsamen Anwendung entworfen werden kann. 
   Bedeutet, ihr gebt mir mindestens vier Code-Dateien ab. Eine für die 
   gemeinsame Konsolenanwendung und je eine pro Teammitglied für ein 
   Dateiformat.
2. Einigt euch auf eine art Schnittstelle zwischen der 
   Hauptanwendung und den Dateimodulen. Es wird wohl eine Liste von 
   Objekten vom Typ Ticket sein.
3. Legt fest, wer welches Dateiformat übernimmt.
4. Erstellt gemeinsam die Konsolenanwendung.
5. Jeder erstellt entsprechend eurer Festlegung die Software für Dateiimport und Export für sein Format.
6. Jedes Teammitglied erstellt eine Kurzdoku zu Dateiformat und Funktion des Import/Export Codes im [Markdown Style](https://de.wikipedia.org/wiki/Markdown).
7. Führt die Anwendung zusammen und testet sie ausführlich.
8. Dokumentiert, in einem MD File, wer welchen Import/Export entwickelt hat.
9. Ladet die Projektdateien als Zip-Archiv über die Aufgabe in Moodle hoch. (Nur einer pro Projektteam.)

#### Bewertung

Die Bewertung des Projekts erfolgt nach folgenden Kriterien und den pro Kriterium zu erreichenden punkten.

| Kriterium                                                                                                                                                      | max. Punkte |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| Gesamtkonzept (Wie ist das Konsolenprogramm aufgebaut, <br>werden Fehler behandelt, wie ist das Thema Schnittstelle zwischen den <br>Modulen gelöst.)          | 5           |
| Code-style Konsolenanwendung (effizienter, aber lesbarer<br> und gut wartbarer Code, Kommentare wenn nötig, aber kein Aufsatz über <br>die Programmiersprache) | 5           |
| Umsetzung Import Code                                                                                                                                          | 5           |
| Umsetzung Export Code                                                                                                                                          | 5           |
| Code-style Import/Export                                                                                                                                       | 5           |
| Bewertung Kurzdoku                                                                                                                                             | 5           |
| Mitarbeit im Team                                                                                                                                              | 5           |
