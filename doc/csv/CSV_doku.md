# CSV

## Spezifikation:

Spezifiziert in RFC 4180.

## Aufbau:

Die CSV enthält eine Titelleiste in der Definiert ist, in welcher Reihenfolge die Parameter vorhanden sind.

Ab der zweiten Zeile findet man in unserem Falle, zeilenweise die einzelnen Tickets.

Die einzelnen Attribute werden durch ";" getrennt.

**Hier ein Beispiel:**

```csv
customer;title;description;state;date
customer0;title0;description0;1;1671314631031
customer1;title1;description1;1;1671314631031
customer2;title2;description2;1;1671314631031
customer3;title3;description3;1;1671314631032
customer4;title4;description4;1;1671314631032
customer5;title5;description5;1;1671314631032
```

## Implementierung

### Import

Zu erst wird die Titelleiste eingelesen um die Reihenfolge der Parameterzu bestimmen.

Danach werden die Tickets zeilenweise eingelesen.

Aus jeder Zeile werden dann die einzelnen Daten der jeweiligen Tickets extrahiert und in den lokalen Storage gepackt.

### Export

Der Export funktioniert genau anders herum

Erst wird die Zeile mit den jeweiligen Spaltenbezeichnungen in die Datei geschrieben

Danach wird für jedes der Tickets eine Zeile mit den zugehörigen Werten geschrieben.