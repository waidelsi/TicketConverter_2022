# Das JSON Format

JSON steht für Java Script Object Notation. Es ist ein Format, in dem man Objekte repräsentieren kann. Für unser Ticketobjekt sind folgende Bausteine relevant

## Beginn eines JSON Dokuments

Nach [RFC-4627](https://www.ietf.org/rfc/rfc4627.txt)  ist die übergeordnete Datenstruktur eines JSON Dokuments entweder ein Array oder Ein Objekt. Alles andere ist in diesen beiden Datenstrukturen enthalten

## Objekt

Um ein Ticket darzustellen, bietet sich der "Objekt" Baustein aus JSON an.

Das Objekt selbst hat keinen Namen.

Er wird gekennzeichnet mit einem `{}`

Ein Objekt enthält Values. Diese werden mit einem String benannt, nach einem `:` folgt ein Wert. Dies Kombination nennt sich Schlüssel-Wert-Paar (Key-Value-Pair). Mehrere hinternander werden mit einem `,` getrennt Ein Beispielobjekt sieht in etwa so aus:

```json
{
    "Wert" : <value>,
    "anderer Wert" : <value>
}
```

Das Objekt hat folgenden Syntax

![json_object.png](assets/json_object.png)

## Array

Ein Array ist eine Liste aus Values, allerdings nur die Werte selbst, kein Schlüssel-Wert paar. Werte werden hier auch mit `,` getrennt. Ein Array beginnt mit `[` und endet mit einem `]`. Diese Datenstruktur bietet sich als Liste für die Ticket-Objekte an.

![json_array.png](assets/json_array.png)

## Value

Value steht für einen Wert. Objekt Dieser kann von verschiedene Typen annehmen für das Ticketsystem sind folgende relevant:

| Name   | Beschreibung                                                                                     | Werte                        |
| ------ | ------------------------------------------------------------------------------------------------ | ---------------------------- |
| string | eine einfache Buchstabenkette, formatiert als `"<Wert>"` unterstützt ASCII escape chars          | title, description, customer |
| number | Eine Nummer mit mögl. nachkommastellen und Exponent. Beginnt mit `-` oder einer `Ziffer von 0-9` | state, dateteime             |
| array  | Eine Liste von mehreren values. Gekennzeichnet durch `[]`                                        | ticketList                   |
| object | siehe Objekt                                                                                     | Ticket                       |

Der Syntax wird mit folgende Diagrammen dargestellt:
![json_string.png](assets/json_string.png)
![json_number.png](assets/json_number.png)
![json_value.png](assets/json_value.png)
