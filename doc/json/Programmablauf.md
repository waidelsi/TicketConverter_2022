# Struktur der JSON-Klasse

## importFile(File f)

Die Import File Extension des JSON Interfaces nimmt eine Datei als Input und versucht deren Inhalt als JSON-Datei zu importieren.

### Struktur

Für den Import der JSON Formate wurde folgende Struktur entworfen:

1. Importiere die Inhalte des Dokuments in einen String

2. Extrahiere jedes JSON-Objekt aus dem String

3. Iteriere über die Liste, für jedes Objekt:
   
   1. ~~Entferne allen Whitespace~~
      
      1. Dieser Teil wurde im Code ausgeschaltet, weil Tickets sonst keine Leerzeichen mehr enthalten haben. Es hat keine größeren Auswirkungen, weil unsere JSON-Exports an den kritischen Stellen keinen Whitespace enthalten 
   
   2. Extrahiere alle Name-Wert-Paare und speichere sie zwischen:
      
      1. title, description, author, customer, state, datetime
   
   3. Erstelle mit den jew. Werten ein neues Ticket

Um die Haupt-Import Methode übersichtlich zu halten, wurden die komplizierteren Einzelschritte in Untermethoden ausgelagert.

### Importieren des Strings

Am Anfang der Methode, wird der Inhalt der gegebenen Datei, zur weiteren Auswertung in einen String importiert.  Das verwendete Beispiel ist unter folgendem Link zu finden:

[https://www.javatpoint.com/how-to-read-file-line-by-line-in-java](https://www.javatpoint.com/how-to-read-file-line-by-line-in-java) 

### Extrahieren von Strings mit Hilfe von regulären Ausdrücken

Um Text aus dem zu analysierenden string zu extrahieren, werwenden wir reguläre Ausdrücke. Diese sind näher in [der Strukturdokumentation](JSON_Struktur_und_Regex.md) näher beschrieben. Sie haben außerdem zum Vorteil, dass am Code nur wenige Änderungen vorgenommen werden müssen, damit ein Projekt-Fremdes JSON-Dokument importiert werden kann, sofern es die notwendigen Felder enthält.

Für die Implementierung wurde folgendes Beispiel zur Orientierung verwendet:

https://jenkov.com/tutorials/java-regex/matcher.html

### Entfernen von Inhalten aus einem String via Regex

Damit die erstellten regulären Ausdrücke richtig funktionieren, müssen sie erstmal von jeglichem Whitespace (Leerzeichen, Tabs, Zeilenumbrüche, usw.) bereinigt werden. 

Hierzu wurde ein Code-Schnipsel von folgender Website verwendet

[https://www.geeksforgeeks.org/how-to-remove-all-white-spaces-from-a-string-in-java/](https://www.geeksforgeeks.org/how-to-remove-all-white-spaces-from-a-string-in-java/)

## ExportFile(f)

Im Vergleich zum Import ist die Export Funktion relativ einfach gehalten. Sie erstellt am Anfang einen String, dem es folgende weitere Strings hinzufügt:

1. Anfang des Arrays mit `[`

2. Für Jedes Ticket
   
   1. Öffnet eines Objekts `{`
   
   2. Fügt jedes Objekt die jeweilegen Attribute als Name-Value-Pair hinzu:
      
      1. `"Name":`+getWert()+`",` 
      
      2. Beim Letzten wird das `,` weggelassen
   
   3. Schließen des Objekts und signalisieren eines neuen `},`

3. Entfernen des letzten Kommas um den letzten Wert im Array zu signalisieren

4. Schließen des Arrays`]`

Anschließend wird dieser String in die gegebene Datei geschrieben
