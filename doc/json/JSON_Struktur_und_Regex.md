# Struktur der JSON Export Datei

Unter Beachtung von [RFC-4627](https://www.ietf.org/rfc/rfc4627.txt) und [ECMA-404](https://www.ecma-international.org/publications-and-standards/standards/ecma-404/), übersichtlicher dokumentiert unter [json.org](https://www.json.org/json-en.html), sowie in der [eigenen Dokumentation](JSON-Format.md) ist folgende Struktur für den Ex- und Import sinnvoll:

- Array mit allen Ticketobjekten
  
  - Ticketobjekte
    
    - **title** (String): Titel des Tickets. `getTitle()`
    - **description**(String): Inhalt des Tickets. `getDescription()`
    - **customer**(String): Name des Erstellers des Tickets `getCustomer()`
    - **state**(number): Status des Tickets, standartmäßig 1 (offen) `getState()`
    - **datetime**(number): Zeitstempel in Millisekunden nach `January 1, 1970, 00:00:00 GMT`  `getDateTime().getTime()`

Ein Beispiel JSON-Dokument nach diesem Layout formatiert ist, sieht folgendermaßen aus:

```json
[
    {
        "title":"Titel1",
        "description":"Beschreibung1",
        "customer":"Ersteller1",
        "state":1,
        "datetime":1671043706000
    },
    {
        "title":"Titel2",
        "description":"Beschreibung2",
        "customer":"Ersteller2",
        "state":1,
        "datetime":1671043706000
    },
    {
        "title":"Titel3",
        "description":"Beschreibung3",
        "customer":"Ersteller3",
        "state":1,
        "datetime":1671043706000
    }
]
```

## Entwurf von Regulären Ausdrücken

Zum extrahieren von bestimmten Elementen aus einem Textdokument, eignen sich reguläre Ausdrücke besonders gut, weil man sehr genau beschreiben kann, was man haben möchte. Sie werden auch oft RegEx genannt. Da sich diese an den JSON Elementen, anstatt an der Gesamtstruktur des Dokumentes orientieren, ist es ohne große Änderungen auch möglich, Projekt-Fremde JSON-Dateien zu importieren.
Die Aufgabe des Regex ist es hier, das gesamte Textdokument auf die jew. definierten Kriterien zu durchsuchen und entsprechende Übereinstimmungen zu extrahieren.

### 1. Objekte extrahieren

folgender Regex extrahiert alle Objekte aus einem beliebigen Dokument

```regex
\{[^\}]*\}
```

Er funktoiniert wiefolgt:

1. `\{` beschreibt den Anfang eines JSON Objekts weil `{` Teil des Syntax von regulären Ausdrücken ist, muss es mit dem Escape-Prefix `\` versehen werden
2. `[^}]*` Steht für "Alles, was nicht `}` (Das Ende des Objekts) ist" es besteht aus folgenden Unterelementen:
   1. `[]` steht für eine Gruppe an Zeichen
   2. `^\{` steht in diesem Kontext für "alles, nur nicht `}`"
   3. `*` steht für beliebig oft
3. `\}` beschreibt das Ende des Objekts

Nach diesem Schritt hat man Extrakte, die jew. so aussehen:

```json
    {
        "title":"Titel1",
        "description":"Beschreibung1",
        "customer":"Ersteller1",
        "state":1,
        "datetime":1671043706000
    }
```

### 2. Entfernen des gesamten Whitspaces

Whitespace (Leerzeichen, Tabs, Zeilenumbrüche,etc.) sich praktisch für Lesbarkeit, jedoch hinderlich beim auslesen. Zum entfernen von jedem Whitespace im Objekt, kann folgender Regex verwendet werden:

```regex
[\s]*
```

1. `[]` Gruppe von Zeichen

2. `\s` Ausdrucksklasse für alle Whitespaces

3. `*` Steht für alle Whitespaces

Das Objekt sieht danach so aus:

```json
{"title":"Titel1","description":"Beschreibung1","customer":"Ersteller1","state":1,"datetime":1671043706000}
```

**Alle Nachfolgenden Ausdrücke gehen davo aus, dass kein Whitespace mehr da ist** 

Für von uns generierte Dateien ist es nicht notwendig, diesen Regex anzuwenden, weil bereits an den kritischen Stellen kein Whitespace vorhanden ist.

### 3. Extrahieren eines gewünschten Name-Value pairs

Nun können die Einzelnen Werte nach Name extrahiert werden

```regex
"name":[^\}\],]*[\}\],]
```

1. `"name":` spezifiziert das gewünschte Schlüsselpar nach der Vorlage (sieht immer so aus: `"schlüssel":`)

2. `[^\}\],]*` Stellt den Wert dar (alles, außer die Trennzeichen `]`,`}` oder `,`) 

3. `[\}\],]` Steht für die Trennzeichen am Ende eines jeden Paars (`]`,`}` oder `,`)

Das extrahierte Paar sieht dann so oder so aus:

```json
1. "title":"Titel1",
2. "datetime":1671043706000}
```

## 4. Extrahieren des Keys

Da für Zahlen und Strings sowieso einzelne Methoden erstellt werden müssen, kann man zur Übersicht auch zwei unterschiedliche Regexes verwenden:

### 4.1 Extrahieren eines Strings

```regex
(?<=:").*(?="[,}\]])
```

1. `(?<=:")` Achtet auf den Prefix `:"` vor dem String steht, was den key-Teil anzeigt, macht ihn aber nicht zum Teil des Matches (Positive Lookbehind)

2. `.*` Matcht den eigentlichen String (z.B `Titel1`)

3. `(?="[,}\]])` Achtet auf das Ende des Strings, matcht ihn aber nicht (positive lookahead)
   
   1. `"` Steht für das Ende des Strings
   
   2. `[,}\]]` Steht für einen der Folgenden JSON-Trennzeichen `,` (neuer Listeneintrag) `}` (Ende Objekt) oder `]` (ende Array) Steht keines dieser Zeichen dahinter, ist es kein valides JSON Objekt

### 4.2 Extrahieren einer Zahl

```regex
(?<=:)\d*(?=[,}\]])
```

1. `(?<=:)` Achtet darauf, dass der Prefix `:` vor dem String steht, was den key-Teil anzeigt, macht ihn aber nicht zum Teil des Matches (Positive Lookbehind)

2. `\d*` Matcht eine Zahlenkette mit Zahlen von 0-9

3. `(?="[,}\]])` Achtet auf das Ende des Strings, matcht ihn aber nicht (positive lookbehind)
   
   1. hinter einer Zahl folgt kein normaler Suffix sondern nur ein JSON-Trenn-Element
   
   2. `[,}\]]` Steht für einen der Folgenden JSON-Trennzeichen `,` (neuer Listeneintrag) `}` (Ende Objekt) oder `]` (ende Array) Steht keines dieser Zeichen dahinter, ist es kein valides JSON Objekt

# Regex Tricks

Hier werden einige spezielle Ausdrücke der Ausdrücke genauer erklärt 

## Positive Lookahead

Wenn ein gesuchtes Element ein bestimmtes Merkmal hinter sich hat, dieses aber nicht gematcht werden soll, kann man dass mit dem Negative Lookahead erreichen

### Bsp:

Gegebener String

```json
"key":1234}
```

Regex:

```regex
.*(?=\})
```

Matcht `"key":1234` **nicht** `"key":1234}` (das `}` wird nicht mitgematcht)

## Positive Lookbehind

Wenn ein gesuchtes Element ein bestimmtes Merkmal vor sich hat, dieses aber nicht gematcht werden soll, kann man dass mit dem Negative Lookahead erreichen

## Bsp:

```json
:"Titel1"
```

Regex:

```regex
(?<=:)".*"
```

Matcht `"Titel1"` **nicht** `:"Titel1"` (das `:` wird nicht mitgematcht)
