# Design des Textbasierten Menus

Wenn der User das Program startet, erhält dieser ein kleines übersichtliches Menu.
Dieses führt in durch die einzelnen Funktionen des Ticketconverters.

Folgende Punkte sind zu finden:

1. Import File -> hier kann der Nutzer eine Datei mit Tickets importieren.
2. Export -> hier kann der Nutzer die Tickets exportieren
3. List Tickets -> Auflistung der gespeicherten Tickets.
4. Clear Tickets -> Löscht alle im Program gespeicherten Tickets.
5. Generate Test Tickets -> Erstellt 25 Test Tickets
6. Create a Ticket -> Erstellt ein Ticket

## 1 - Import File

Hier kann eine vorhandene datei mit Tickets importiert werden.
Dazu wird vom Benutzer die Eingabe einer Datei mit Pfad entgegen genommen.
Durch die Dateiendung erkennt das Program automatisch um welchen Dateityp es sich handelt.
Und importiert die Tickets entsprechend.

## 2 - Export

Hier werden die Tickets in eine Datei exportiert.
Dazu wird vom Benutzer die Eingabe einer Datei mit Pfad entgegen genommen.
Durch die Dateiendung erkennt das Program automatisch um welchen Dateityp es sich handelt.
Und exportiert die Tickets entsprechend.

## 3 - List Tickets

Hier werden die Tickets seitenweise aufgelistet.
Pro Seite werden 10 Tickets angezeigt.
Duch die Benutzereingabe 0 kommt man wieder zum Hauptmenu.

## 4 - Clear Tickets

Um vorhandene Tickets zu löschen ist dieser Menupunkt vorgesehen.
Hier können alle Tickets auf einmal gelöscht werden

## 5 - Generate Test Tickets

Um ein paar Beispieltickets zu haben, generiert diese Methode 25 test Tickets.

## 6 - Create a Ticket

Hier kann ein Ticket erstellt werden.
Dazu werden folgende Informationen vom User engegen genommen:

1. Titel des Tickets
2. Kunde des Tickets
3. Die Beschreibung des Tickets.
