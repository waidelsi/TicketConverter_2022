# Aufbau des Ticket.xml

Das [XML-Dokument](XML-Format.md) ist wiefolgt aufgebaut:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ticketList>
    <ticket>
        <title>Titel1</title>
        <description>Beschreibung1</description>
        <customer>Ersteller1</customer>
        <state>1</state>
        <datetime>1671043706000</datetime>
    </ticket>
    <ticket>
        <title>Titel2</title>
        <description>Beschreibung2</description>
        <customer>Ersteller2</customer>
        <state>1</state>
        <datetime>1671043706000</datetime>
    </ticket>
    <ticket>
        <title>Titel3</title>
        <description>Beschreibung3</description>
        <customer>Ersteller3</customer>
        <state>1</state>
        <datetime>1671043706000</datetime>
    </ticket>
</ticketlist>
```

Das Dokument beginnt mit Metadaten, die das nachfolgende Dokument beschreiben. Hier wird Version (1.0), Encoding (UTF-8) und Typ (alleinstehend) angegeben. Dieser Block nennt sich XML-Deklaration.

Das Ticket beginnt mit dem Wurzelobjekt, hier heißt es `ticketList`.

Im Wurzelelement sind die Ticketelemente mit folgenden Unterelementen vorhanden:

| Element     | Funktion                            |
| ----------- | ----------------------------------- |
| title       | Titel des Tickets                   |
| description | Beschreibung des Tickets            |
| customer    | Ersteller des Tickets               |
| state       | Status des Tickets                  |
| datetime    | Timestamp des Erstellungszeitpunkts |
