# XML Aufbau

## Ticket-Export

Der Ticket-Export in XML-Dateien wird wie folgt ausgeführt:

Zuerst wird das Dokument mit XML-Metadaten angereichert und dann mit dem Wurzelelement `<ticketList>` initialisiert

Als nächstes wird für jedes Ticket in der Liste folgendes gemacht:

1. Ticket Element `<ticket>` öffnen

2. Alle Attribute des Tickets hinzufügen

3. Ticket Element mit `</ticket>` schließen

Zum Schluss wird das Dokument-Ende mit `</ticketList>` geschlossen. 

Danach wird der FileWriter geschlossen. Es folgt noch eine Catch-Anweisung, falls Probleme und/oder Fehler während dem Ausführen auftreten.

## Ticket-Import

Der Ticket-Import in XML-Dateien wird wie folgt ausgeführt:

Als erstes weiden so lange Zeilen eigelesen, bis das erste Ticket anfängt. Dies wird dadurch erkannt, dass in der Zeile "<ticket>" vorkommt.

Dann werden solange die Parameter des Tickets eingelesen, bis man alle Parameter eingelesen hat. Das ende des Tickets wird dadurch erkannt, dass in der Zeile "</ticket>" vorkommt.

Wurden die Parameter eines Tickets eingelesen, dann wird aus diesen Parametern das Ticketobjekt gebaut und in den Internen Storage gepackt.

Danach geht es solange zeilenweise weiter bis das nächste Ticket beginnt.

Dann werden wieder alle Ticketparameter eingelesen.

Der Export endet, sobald das Ende des Dokumentes erreicht wurde
