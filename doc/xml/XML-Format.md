# Das XML Format

XML steht für Extensible Markup Language. Sie ist eine Auszeichnungsprache und wird zur hierarichischen Darstellungen für Mensch und Maschine benuzt.

## Aufbau einer XML-Datei

Der minimale XML-Aufbau besteht aus Start-Tag, Inhalt, End-Tag und Wurzelelement:

```
<Auto>
    <Marke>Audi</Marke>
    <Preis>12000</Preis>
    <Name>R8</Name>
</Auto>
```

Hier sind jeweils <Auto>, <Preis> und <Name> die Start-Tags sie geben den Start einens XML-Elements an und benennt diese Element direckt.

End-Tags werden durch das / signalisiert, hier in diesem Beispiel wären das </Marke>, </Preis>, </Name> und </Auto>, sieh stehen am Ende des Elements und schließen dieses ab.

Der Inhalt des XML-Elements steht zwischen dem Start- und End-Tag, hier im Beispiel Audi, 12000 und R8. Hier werden die zu vermittelnden Daten hinterlegt.

Ein Wurzelelement ist das Element das alle anderen Element umfasst und keinen richtigen Inhalt besitzt, hier wäre das <Auto></Auto> Im Dokument darf nur eines existieren.

## Attribute

Attribute können verwendet werden um ein Element eindeutiger zu deklarieren, sie werden wie folgt deklariert:

```
<Preis preistyp="Listenpreis">12000</Preis>
```

Es ist auch möglich mehrere Attribute für ein Element festzulegen.

## XML-Deklaration

Optional kann auch noch eine Verarbeitungsanweisungen in die XML eingefügt werden diese gibt an wie die XML dargestllt werden soll.

Dies kann wie folgt aussehen:

```
<?xml:stylesheet type="text/xsl" href="stylesheets/print.xsl" ?>
```

EIne Verarbeitungsanweisungen beginnt immer mit <?, danach kommt das Target, hier "xml:stylesheet", anhand dessen kann dann entschieden werden ob die Verarbeitungsanweisung interpretiert werden kann. Danach folgt der eigentliche Inhalt der Anweisung folgt danach, abgeschlossen wird es mit ?>.
