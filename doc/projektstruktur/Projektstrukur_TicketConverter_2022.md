# Projektstrukur TicketConverter_2022

Das Projekt ist wiefolgt aufgebaut:

![](Aufbau.jpg)

## Menu

Das Herzstück des Projekts. Von hier wird das Menu geladen und, je nach Nutzereingabe die anderen Funktionen aufgerufen. Es ist in der Klasse `Menu.java` implementiert wird durch `Programmstart.java` initialisiert und gestartet.

## Interface

Eine Abstrahierung der Einzelnen Formatklassen. Es wird durch die Datei `Extensions.java` definiert. Es definiert zwei Methoden:

- **exportFile(File f)**: Exportiert in eine Datei

- **importFile(File f)**: Importiert in eine Datei

Das Interface implementiert diese Methoden allerdings nicht, sondern stellt nur sicher, dass jede Klasse, die dieses Implementiert, diese Methoden hat.

Dadurch können ohne großen Aufwand weitere Dateitypen hinzugefügt werden

## Json/CSV/XML

Die jeweiligen Implementierungen der jeweiligen Formate. Hier sind die import und export Funktionen für die jew. Dateiendungen implementiert.

Sie sind in den Dateien `Json.java`,`CSV.java` und `XML.java`

## Ticket

Die Ticket-Klasse definiert das Ticket mit allen Attributen und Methoden.

Sie kommt mit Konstruktoren zum normalen Erstellen, als auch zum Importieren von bereits erstellten Tickets (damit State und Zeitstempel erhalten bleiben)

Die Attribute des Tickets sehen wiefolgt aus:

| Name        | Typ                | Beschreibung                                                                                       | get-Methode      |
| ----------- | ------------------ | -------------------------------------------------------------------------------------------------- | ---------------- |
| title       | String             | Titel oder Betreff des Tickets                                                                     | getTitle()       |
| description | String             | Inhalt des Tickets                                                                                 | getDescription() |
| customer    | String             | Ersteller des Tickets                                                                              | getCustomer()    |
| state       | int                | Status des Tickets, 1 für "offen", 2 für "in Bearbeitung", 3 für "geschlossen"                     | getState()       |
| datetime    | java.sql.Timestamp | Die genaue Uhrzeit und Datum, an dem das Ticket erstellt wurde. Wird als Zeitstempel abgespeichert | getDatetime()    |

## Storage

Die Storage Klasse ist für das Speichern von allen Tickets im Programmablauf zuständig. Sie ist in der Datei `Storage.java` Hier werden Tickets gespeichert und abgerufen. Alle ihre Methoden sind statisch, was bedeutet, dass nirgendwo ein Objekt generiert werden muss, was es einfacher macht, Tickets von überall aus dem Programm abzurufen. 

Gespeichert werden die Tickets in einer ArrayList vom Typ Ticket. Mit den nachfolgenden Methoden, können Tickets aus der Liste gelesen und in die Liste gespeichert werden.
